"""
В этом задании в форме регистрации требуется загрузить текстовый файл.
Напишите скрипт, который будет выполнять следующий сценарий:
Открыть страницу https://suninjuly.github.io/file_input.html
Заполнить текстовые поля: имя, фамилия, email
Загрузить файл. Файл должен иметь расширение .txt и может быть пустым
Нажать кнопку "Submit"
Если все сделано правильно и быстро, вы увидите окно с числом.
Отправьте полученное число в качестве ответа для этого задания.
"""


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    import os
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/file_input.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # находим и устанавливаем значение для поля: Имя
        chrome_browser.find_element(By.NAME, 'firstname').send_keys('Andrew')
        # находим и устанавливаем значение для поля: Фамилия
        chrome_browser.find_element(By.NAME, 'lastname').send_keys('Ivanov')
        # находим и устанавливаем значение для поля: Почта
        chrome_browser.find_element(By.NAME, 'email').send_keys('IvanovAA@mail.com')
        # путь к тестовому файлу
        file_name: str = os.path.join("C:\\", "WORK", "Download", "test_dl.txt")
        # находим селектор файла передаем туда - имя файла
        chrome_browser.find_element(By.ID, 'file').send_keys(file_name)
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
