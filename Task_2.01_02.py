"""
В данной задаче вам нужно с помощью роботов решить ту же математическую задачу, как и в прошлом задании.
Но теперь значение переменной х спрятано в "сундуке", точнее, значение хранится в атрибуте valuex у картинки
с изображением сундука.
Ваша программа должна:
Открыть страницу https://suninjuly.github.io/get_attribute.html.
Найти на ней элемент-картинку, который является изображением сундука с сокровищами.
Взять у этого элемента значение атрибута valuex, которое является значением x для задачи.
Посчитать математическую функцию от x (сама функция остаётся неизменной).
Ввести ответ в текстовое поле.
Отметить checkbox "I'm the robot".
Выбрать radiobutton "Robots rule!".
Нажать на кнопку "Submit".
Для вычисления значения выражения в п.4 используйте функцию calc(x) из предыдущей задачи.
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени), вы
увидите окно с числом. Скопируйте его в поле ниже и нажмите кнопку "Submit", чтобы получить баллы за задание.
"""


def calc(x: str) -> str:
    """
     Функция расчета f(x). Где f = ln(abs(12*sin(x)))

     Параметры:
         x: число в виде текста.

     Возвращаемое значение:
         return: расчитанное значение f(x) в текстовом формате
     """
    import math
    return str(math.log(abs(12 * math.sin(int(x)))))


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/get_attribute.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # считываем значние переменной x - на открывшейся странице из
        # атрибута(с именем 'valuex') картинки сундучка
        x = chrome_browser.find_element(By.ID, 'treasure').get_attribute('valuex')
        # высчитываем f(x)
        x = calc(x)
        # находим поле на странице для вывода ответа
        answer = chrome_browser.find_element(By.ID, 'answer')
        # выводим в найденное поле значение из x
        answer.send_keys(x)
        # находим и прожимаем checkBox - "I'm the robot"
        chrome_browser.find_element(By.ID, 'robotCheckbox').click()
        # находим и прожимаем radioButton - "Robots rule!"
        chrome_browser.find_element(By.ID, 'robotsRule').click()
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
