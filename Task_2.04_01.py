"""
Задание: ждем нужный текст на странице
Попробуем теперь написать программу, которая будет бронировать нам дом для отдыха по строго заданной
цене. Более высокая цена нас не устраивает, а по более низкой цене объект успеет забронировать кто-то другой.
В этой задаче вам нужно написать программу, которая будет выполнять следующий сценарий:
Открыть страницу https://suninjuly.github.io/explicit_wait2.html
Дождаться, когда цена дома уменьшится до $100 (ожидание нужно установить не меньше 12 секунд)
Нажать на кнопку "Book"
Решить уже известную нам математическую задачу (используйте ранее написанный код) и отправить решение
Чтобы определить момент, когда цена аренды уменьшится до $100,
используйте метод text_to_be_present_in_element из библиотеки expected_conditions.
"""


def calc(x: str) -> str:
    """
     Функция расчета f(x). Где f = ln(abs(12*sin(x)))

     Параметры:
         x: число в виде текста.

     Возвращаемое значение:
         return: расчитанное значение f(x) в текстовом формате
     """
    import math
    return str(math.log(abs(12 * math.sin(int(x)))))


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions
    import time
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/explicit_wait2.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # находим цену на Дом и ждем (макисмум 10 секунд) пока она не опустится до 100
        wait = WebDriverWait(chrome_browser, 10)
        wait.until(expected_conditions.text_to_be_present_in_element((By.ID, 'price'), '100'))
        # находим и нажимаем кнопку Book
        chrome_browser.find_element(By.ID, 'book').click()
        # пауза в 1 секунда
        time.sleep(1)
        # считываем значние переменной x
        x = chrome_browser.find_element(By.ID, 'input_value').text
        # находим поле на странице для вывода ответа и выводим в найденное поле значение f(x)
        chrome_browser.find_element(By.ID, 'answer').send_keys(calc(x))
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.ID, 'solve').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
