"""
В этой задаче вам нужно написать программу, которая будет выполнять следующий сценарий:
Открыть страницу https://suninjuly.github.io/alert_accept.html
Нажать на кнопку
Принять confirm
На новой странице решить капчу для роботов, чтобы получить число с ответом
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени),
вы увидите окно с числом. Отправьте полученное число в качестве ответа на это задание.
"""


def calc(x: str) -> str:
    """
     Функция расчета f(x). Где f = ln(abs(12*sin(x)))

     Параметры:
         x: число в виде текста.

     Возвращаемое значение:
         return: расчитанное значение f(x) в текстовом формате
     """
    import math
    return str(math.log(abs(12 * math.sin(int(x)))))


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/alert_accept.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # находим и нажимаем кнопку "I want to go on a magical journey!"
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
        # жмем ОК в окне confirm
        chrome_browser.switch_to.alert.accept()
        # пауза в 1 секунду
        time.sleep(1)
        # считываем значние переменной x - на открывшейся странице
        x = chrome_browser.find_element(By.ID, 'input_value').text
        # находим поле на странице для вывода ответа и
        # выводим в найденное поле значение из f(x)
        chrome_browser.find_element(By.ID, 'answer').send_keys(calc(x))
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
