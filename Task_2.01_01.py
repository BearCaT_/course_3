"""
Задание: кликаем по checkboxes и radiobuttons (капча для роботов)
Продолжим использовать силу роботов 🤖 для решения повседневных задач. На данной странице мы добавили капчу
для роботов, то есть тест, являющийся простым для компьютера, но сложным для человека.
Ваша программа должна выполнить следующие шаги:
Открыть страницу https://suninjuly.github.io/math.html.
Считать значение для переменной x.
Посчитать математическую функцию от x (код для этого приведён ниже).
Ввести ответ в текстовое поле.
Отметить checkbox "I'm the robot".
Выбрать radiobutton "Robots rule!".
Нажать на кнопку Submit.
Для этой задачи вам понадобится использовать атрибут .text для найденного элемента. Обратите внимание,
что скобки здесь не нужны:
x_element = browser.find_element(By.CSS_SELECTOR, selector_value)
x = x_element.text
y = calc(x)
Атрибут text возвращает текст, который находится между открывающим и закрывающим тегами элемента.
Например, text для данного элемента <div class="message">У вас новое сообщение.</div> вернёт строку:
"У вас новое сообщение".
Используйте функцию calc(), которая рассчитает и вернет вам значение функции, которое нужно ввести
в текстовое поле. Не забудьте добавить этот код в начало вашего скрипта:
import math
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени),
вы увидите окно с числом. Скопируйте его в поле ниже.
"""

def main() -> None:
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    import math
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/math.html'
    try:
        # инициализация драйвера браузера
        browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # считываем значние переменной x - на открывшейся странице
        x = browser.find_element(By.ID, 'input_value')
        # высчитываем f(x)
        x = str(math.log(abs(12 * math.sin(int(x.text)))))
        # находим поле на странице для ввода ответа
        answer = browser.find_element(By.ID, 'answer')
        # вводим в поле значение x
        answer.send_keys(x)
        # находим и прожимаем checkBox - "I'm the robot"
        browser.find_element(By.ID, 'robotCheckbox').click()
        # находим и прожимаем radioButton - "Robots rule!"
        browser.find_element(By.ID, 'robotsRule').click()
        # находим и нажимаем кнопку Submit
        browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 20 секунд
        time.sleep(20)
        # закрываем браузер после всех манипуляций
        browser.quit()


if __name__ == "__main__":
    main()

