"""
В данной задаче вам нужно будет снова преодолеть капчу для роботов и справиться с ужасным и
огромным футером, который дизайнер всё никак не успевает переделать. Вам потребуется написать код, чтобы:
Открыть страницу https://SunInJuly.github.io/execute_script.html.
Считать значение для переменной x.
Посчитать математическую функцию от x.
Проскроллить страницу вниз.
Ввести ответ в текстовое поле.
Выбрать checkbox "I'm the robot".
Переключить radiobutton "Robots rule!".
Нажать на кнопку "Submit".
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени),
вы увидите окно с числом. Отправьте полученное число в качестве ответа для этого задания.
Для этой задачи вам понадобится использовать метод execute_script, чтобы
сделать прокрутку в область видимости элементов, перекрытых футером.
"""


def calc(x: str) -> str:
    """
     Функция расчета f(x). Где f = ln(abs(12*sin(x)))

     Параметры:
         x: число в виде текста.

     Возвращаемое значение:
         return: расчитанное значение f(x) в текстовом формате
     """
    import math
    return str(math.log(abs(12 * math.sin(int(x)))))


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/execute_script.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # считываем значние переменной x - на открывшейся странице из
        # атрибута(с именем 'valuex') картинки сундучка
        x = chrome_browser.find_element(By.ID, 'input_value').text
        # высчитываем f(x)
        x = calc(x)
        # находим поле на странице для вывода ответа
        answer = chrome_browser.find_element(By.ID, 'answer')
        # выводим в найденное поле значение из x
        answer.send_keys(x)
        # выполняем js скрипт по прокрутке открытой страницы на 100 пикселей вниз
        chrome_browser.execute_script("window.scrollBy(0, 100);")
        # находим и прожимаем checkBox - "I'm the robot"
        chrome_browser.find_element(By.ID, 'robotCheckbox').click()
        # находим и прожимаем radioButton - "Robots rule!"
        chrome_browser.find_element(By.ID, 'robotsRule').click()
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
