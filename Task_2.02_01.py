"""
Для этой задачи мы придумали еще один вариант капчи для роботов. Придется немного
переобучить нашего робота, чтобы он справился с новым заданием.
Напишите код, который реализует следующий сценарий:
Открыть страницу https://suninjuly.github.io/selects1.html
Посчитать сумму заданных чисел
Выбрать в выпадающем списке значение равное расчитанной сумме
Нажать кнопку "Submit"
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени),
вы увидите окно с числом. Отправьте полученное число в качестве ответа для этого задания.
Когда ваш код заработает, попробуйте запустить его на странице https://suninjuly.github.io/selects2.html.
Ваш код и для нее тоже должен пройти успешно.
"""


def main() -> None:
    # Тестовая функция.
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import Select
    import time
    # сумма двух чисел на странице
    summa = 0
    # драйвер браузера
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/selects2.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # считываем значние переменных num1 и num2 на открывшейся странице и находим их сумму
        summa += int(chrome_browser.find_element(By.ID, 'num1').text)
        summa += int(chrome_browser.find_element(By.ID, 'num2').text)
        # находим элемент с выпадающим списком и
        # выбираем значение из списка - равное расчитанной сумме
        Select(chrome_browser.find_element(By.ID, 'dropdown')).select_by_value(str(summa))
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
