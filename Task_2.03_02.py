"""
В этом задании после нажатия кнопки страница откроется в новой вкладке,
нужно переключить WebDriver на новую вкладку и решить в ней задачу.
Сценарий для реализации выглядит так:
Открыть страницу https://suninjuly.github.io/redirect_accept.html
Нажать на кнопку
Переключиться на новую вкладку
Пройти капчу для робота и получить число-ответ
Если все сделано правильно и достаточно быстро (в этой задаче тоже есть ограничение по времени),
вы увидите окно с числом. Отправьте полученное число в качестве ответа на это задание.
"""


def calc(x: str) -> str:
    """
     Функция расчета f(x). Где f = ln(abs(12*sin(x)))

     Параметры:
         x: число в виде текста.

     Возвращаемое значение:
         return: расчитанное значение f(x) в текстовом формате
     """
    import math
    return str(math.log(abs(12 * math.sin(int(x)))))


def main() -> None:
    # Тестовая функция
    from webdriver_manager.chrome import ChromeDriverManager
    from selenium.webdriver.chrome.service import Service
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    import time
    chrome_browser = None
    # адрес ссылки для открытия в браузере
    link = 'https://suninjuly.github.io/redirect_accept.html'
    try:
        # инициализация драйвера браузера
        chrome_browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        # открываем страницу по указанной выше ссылке
        chrome_browser.get(link)
        # пауза в 1 секунду
        time.sleep(1)
        # находим и нажимаем  "летающую" кнопку
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
        # пауза в 1 секунду
        time.sleep(1)
        # переключаемся на новую открывшуюся вкладку в браузере
        chrome_browser.switch_to.window(chrome_browser.window_handles[1])
        # считываем значние переменной x - на открывшейся странице
        x = chrome_browser.find_element(By.ID, 'input_value').text
        # находим поле на странице для вывода ответа и выводим в найденное поле значение f(x)
        chrome_browser.find_element(By.ID, 'answer').send_keys(calc(x))
        # находим и нажимаем кнопку Submit
        chrome_browser.find_element(By.TAG_NAME, 'button').click()
    finally:
        # пауза в 10 секунд
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        if chrome_browser is not None:
            chrome_browser.quit()


if __name__ == "__main__":
    main()
